%% Copyright © 2014 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(instamo1).

%% instamo1: 

-export([ main/0
        , player_actor/1
        , z3_actor/0
        , singer_actor/1
        ]).


-define(z1, 'z1@192.168.1.6').
-define(z2, 'z2@192.168.1.8').
-define(z3, 'z3@192.168.1.6').

%% API

main () ->
    connect_nodes(),
    send_code(),
    File = "wvgRzh6.jpg",
    P  = spawn(?z1, ?MODULE, player_actor, [File]),
    S  = spawn(?z2, ?MODULE, singer_actor, [File]),
    Z3 = spawn(?z3, ?MODULE, z3_actor, []),
    send_pids([{player,P}, {singer,S}, {z3,Z3}]).

%% Internals

player_actor (Actors, File) -> %% retina
    receive
        go ->
            vlc("../../videos/Boko Haram leader_ _I abducted your girls_.mp4"),
            timer:sleep(6*1000),
            vlc(stop),
            {_, S} = lists:keyfind(singer, 1, Actors),
            {_, Z3} = lists:keyfind(z3, 1, Actors),
            [A ! go || A <- [S, Z3]]
    end.

z3_actor (_Actors) -> %% celine
    receive
        go ->
            vlc("../../videos/New Boko Haram video claims to show kidnapped girls.mp4"),
            timer:sleep(15*1000),
            vlc(stop)
    end.

singer_actor (Actors, _File) -> %% rπ
    omxplayer("'../../videos/Hito Steyerl, STRIKE. 2010, 28s, HDV..mp4'"),
    timer:sleep(11*1000),
    {_, P} = lists:keyfind(player, 1, Actors),
    P ! go,
    timer:sleep(16*1000),
    omxplayer(stop),
    receive
        go ->
            omxplayer("--pos 15 '../../videos/Marina Grzinic at 5uper.net Coded Cultures 2009 (artist inte.mp4'"),
            timer:sleep(23*1000),
            omxplayer(stop)
    end.

omxplayer (stop) ->
    os:cmd("killall -9 omxplayer.bin");
omxplayer (Args) ->
    spawn(fun () -> os:cmd("omxplayer "++ Args) end),
    timer:sleep(5000).

vlc (stop) ->
    os:cmd("killall -9 VLC");
vlc (File) ->
    spawn(fun () -> os:cmd("/Applications/VLC.app/Contents/MacOS/VLC --fullscreen '"++ File ++"'") end),
    timer:sleep(200).

sox (play, File) ->
    os:cmd("cat 'priv/"++ File ++"' "
           "| sox -traw -r44100 -b8 -e unsigned-integer - -talsa");
sox (stop, _File) ->
    os:cmd("killall -9 sox").

stop (Actors) ->
    io:format("Stopping...\n"),
    lists:foreach(
      fun (Pid) -> Pid ! stop end,
      Actors).

player_actor (File) ->
    receive
        {pids, List} ->
            player_actor(List, File)
    end.
z3_actor () ->
    receive
        {pids, List} ->
            z3_actor(List)
    end.
singer_actor (File) ->
    receive
        {pids, List} ->
            singer_actor(List, File)
    end.

send_pids (PIDs) ->
    lists:foreach(fun ({_,Pid}) -> Pid ! {pids,PIDs} end, PIDs).

send_code () ->
    Res = [begin
               {Mod, Bin, File} = code:get_object_code(Mod),
               {_ResL, _BadNodes} = rpc:multicall(code, load_binary, [Mod, File, Bin])
           end || Mod <- [?MODULE]],
    Fails = keyfoldl(fun lists:append/2, [], 2, Res),
    io:format("Unreachable nodes: ~p\n", [Fails]),
    Res.

keyfoldl (Fun, AccIn, Key, Tuples) ->
    lists:foldl(fun (Tuple, Acc) ->
                        Elem = element(Key, Tuple),
                        Fun(Elem, Acc)
                end, AccIn, Tuples).

connect_nodes () ->
    Nodes = [ ?z1, ?z2, ?z3 ],
    O = [{Node,net_kernel:connect_node(Node)} || Node <- Nodes],
    io:format("O = ~p\n", [O]).

%% End of Module.
