#installations • [BitBucket](//bitbucket.org/fenollp/installations)

Framework pour installations artistiques

* Essaims de machines bon marché: [Raspberry π](http://www.raspberrypi.org/)
* Communication entre nœuds en réseau fermé
* Utilise [`sox`](http://linuxcommand.org/man_pages/sox1.html) pour l'audio
* Utilise [`vlc`](http://www.videolan.org/doc/vlc-user-guide/en/ch04.html) pour la vidéo

## demo1

1. Lance 2 nœuds sur une même machine
1. `N1` joue un film en boucle & dit à `N2` de jouer un son toutes les `x` secondes
1. On arrète la demo avec la commande `stop`
